<?php session_start() ?>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
        <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet">
<!--        <link rel="stylesheet" href="css/starter-template.css">-->
        <link rel="stylesheet" href="css/style.css">
        <title></title>
    </head>
    <body>

        <nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
            <a class="navbar-brand" href="index.php">Blogi</a>
            <div class="collapse navbar-collapse" id="navbarsExampleDefault">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="index.php">Etusivulle</a>
                    </li>
                    <?php if (isset($_SESSION['kayttaja_id'])) {?>
                    <li class="nav-item"><a class="nav-link" href="lisaa.php">Lisää kirjoitus</a></li>
                    <?php } ?>
                    <?php if (isset($_SESSION['kayttaja_id'])) {?>
                    <li class="nav-item"><a class="nav-link" href="ulos.php">Kirjaudu ulos</a></li>
                    <?php } else { ?>
                    <li class="nav-item"><a class="nav-link" href="kirjaudu.php">Kirjaudu</a></li>
                    <?php } ?>
                </ul>
            </div>
        </nav>