<?php
include_once 'inc/top.php';
$viesti = "";
$tietokanta = new PDO('mysql:host=localhost;dbname=blogi;charset=utf8', 'root', '');
if($_SERVER['REQUEST_METHOD']==='POST') {
    if($tietokanta!=null) {
        try {
            $tunnus = filter_input(INPUT_POST, 'tunnus',FILTER_SANITIZE_STRING);
            $salasana = md5(filter_input(INPUT_POST, 'salasana',FILTER_SANITIZE_STRING));
            
            $sql="SELECT * FROM kayttaja WHERE tunnus ='$tunnus' AND salasana='$salasana'";
            $kysely = $tietokanta->query($sql);

            if ($kysely->rowCount() === 1) {
                $tietue = $kysely->fetch();
                $_SESSION['kirjaudu'] = true;
                $_SESSION['kayttaja_id'] = $tietue['id'];
                header('Location: index.php');
            }
            else {
                $viesti = "Väärä tunnus tai salasana!";
                print $viesti;
                        }
        } catch (PDOException $pdoex) {
            print "Käyttäjän tietojen hakeminen epäonnistui." . $pdoex->getMessage();
        }
    }
}
?>
<form action="<?php print $_SERVER['PHP_SELF']; ?>" method="post">
    <div class="form-group">
        <label for="tunnus">Tunnus:</label>
        <input type="text" class="form-control" name="tunnus">
    </div>
    <div class="form-group">
        <label for="salasana">Salasana:</label>
       <input type="password" class="form-control" name="salasana">
    </div>
    <button type="submit" class="btn btn-default">Kirjaudu</button>
</form>
<?php include_once 'inc/bottom.php'; ?>
