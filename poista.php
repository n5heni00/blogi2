<?php include_once 'inc/top.php';?>

         <?php
        $id = filter_input(INPUT_GET, 'id',FILTER_SANITIZE_NUMBER_INT);
        
        try {
            $tietokanta = new PDO('mysql:host=localhost;dbname=blogi;charset=utf8','root','');
            $tietokanta->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            
            $kysely = $tietokanta->prepare("DELETE FROM kommentti WHERE :id=kirjoitus_id; DELETE FROM kirjoitus WHERE id=:id");

            $kysely->bindValue(':id', $id, PDO::PARAM_INT);
            
            if ($kysely->execute()){
                print('<p>Kirjoitus on poistettu!</p>');
            }
            else {
                print_r($tietokanta->errorInfo());
            }
            print("<a href='index.php'>Etusivulle</a>");
            
            
        } catch (PDOException $pdoex) {
            print '<p>Tietokannan avaus epäonnistui.' . $pdoex->getMessage() . '<p>';
        }
        ?>

    <?php include_once 'inc/bottom.php';?>'