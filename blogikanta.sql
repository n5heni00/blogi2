drop DATABASE if EXISTS blogi;

create DATABASE blogi;

use blogi;

create table kayttaja (
    id int PRIMARY KEY AUTO_INCREMENT,
    sukunimi varchar(50) not null,
    etunimi varchar(50) not null,
    tunnus varchar(50) not null, 
    salasana varchar(50) not null,
    email varchar(50) not null
);

create TABLE kirjoitus (
    id int PRIMARY KEY AUTO_INCREMENT,
    otsikko varchar(50) not null,
    teksti text not null,
    kayttaja_id int not null,
    FOREIGN KEY (kayttaja_id) REFERENCES kayttaja(id)
    on delete RESTRICT,
    paivays timestamp DEFAULT CURRENT_TIMESTAMP
    on UPDATE CURRENT_TIMESTAMP
);

CREATE table kommentti(
    id int PRIMARY KEY AUTO_INCREMENT,
    teksti text not null,
    paivays timestamp DEFAULT CURRENT_TIMESTAMP
    on UPDATE CURRENT_TIMESTAMP,
    kirjoitus_id int not null,
    FOREIGN KEY (kirjoitus_id) REFERENCES kirjoitus(id)
    on DELETE RESTRICT,
    kayttaja_id int not null,
    foreign key (kayttaja_id) references kayttaja(id)
    on delete restrict
);

INSERT INTO kayttaja (sukunimi, etunimi, tunnus, salasana, email) VALUES ('Juntunen','Jouni','jjuntune',md5('testi123'),'jjuntune@oamk.fi');