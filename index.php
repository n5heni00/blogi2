<?php include_once 'inc/top.php'; ?>
<?php

try {
    $tietokanta = new PDO('mysql:host=localhost;dbname=blogi;charset=utf8', 'root', '');

    $tietokanta->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $sql = 'SELECT *,kirjoitus.id as id FROM kirjoitus '
            . 'INNER JOIN kayttaja ON kirjoitus.kayttaja_id = kayttaja.id'
            . ' ORDER BY paivays desc';

    $kysely = $tietokanta->query($sql);

    if ($kysely) {

        while ($tietue = $kysely->fetch()) {

            print '<div class="form-group">';
            print '<p>' . date("d.m.Y H.i", strtotime($tietue['paivays'])) . ' by ' . $tietue['tunnus'] . '</p>';
            print '<a href="blogi.php?id=' . $tietue['id'] . '">' . '<b>' . $tietue['otsikko'] . '</b>';
            if (isset($_SESSION['kayttaja_id'])) {
                print '</a> &nbsp; <a class="glyphicon glyphicon-trash" aria-hidden="true" href="poista.php?id=' . $tietue['id'] . '">' . '</a>';
            }
            print '<hr>';
            print '</div>';
        }
    } else {
        print '<p>';
        print_r($tietokanta->errorInfo());
        print '</p>';
    }
} catch (PDOException $pdoex) {
    print '<p>Tietokannan avaus epäonnistui.' . $pdoex->getMessage() . '</p>';
}
?>
<?php include_once 'inc/bottom.php'; ?>