<?php include_once 'inc/top.php'; ?>
<a href="index.php">Takaisin etusivulle</a>

<?php
$kirjoitus_id = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_NUMBER_INT);
$kayttaja_id = 1;
$_SESSION['kirjoitus_id'] = $kirjoitus_id;
try {
    if ($_SERVER['REQUEST_METHOD'] === 'GET') {
        $tietokanta = new PDO('mysql:host=localhost;dbname=blogi;charset=utf8', 'root', '');

        $tietokanta->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $teksti = filter_input(INPUT_POST, 'kommentti', FILTER_SANITIZE_STRING);
        $sql = $sql = "SELECT *,kirjoitus.id as id FROM kirjoitus INNER JOIN kayttaja ON kirjoitus.kayttaja_id = kayttaja.id"
                . " WHERE kirjoitus.id=$kirjoitus_id"
                . " ORDER BY paivays desc";
        $kysely = $tietokanta->query($sql);
        if ($kysely) {
            $tietue = $kysely->fetch();
            $kayttaja = $tietue['tunnus'];
            $paivays = $tietue['paivays'];
            $date = new DateTime($paivays);
            print '<div class="form-group">';
            print '<h1><strong>' . $tietue['otsikko'] . '</strong></h1>';
            print '<p>' . $date->format('d-m-Y H:i') . ' by ' . $tietue['tunnus'] . '</p>';
            print '<p>' . $tietue['teksti'] . '</p>';
            print '<p><strong>Kommentit</strong></p>';
            print '</div>';
        } else {
            print '<p>';
            print_r($tietokanta->errorInfo());
            print '</p>';
        }
    }
} catch (PDOException $pdoex) {
    print '<p>Tietokannan avaus epäonnistui.' . $pdoex->getMessage() . '</p>';
}
if (isset($_SESSION['kayttaja_id'])) { ?>
    <form id="lisaa_kommentti" action="<?php print($_SERVER['PHP_SELF']); ?>" method="get">
        <input type="hidden" name="id" value="<?php print $kirjoitus_id ?>">    
        <textarea  name="teksti" id="teksti" required="required"></textarea></br>
        <input type="submit" value="Lähetä kommentti">
    </form></br>


<?php
}
if (isset($_GET['teksti'])) {
    if ($_SERVER['REQUEST_METHOD'] === 'GET') {
        $kirjoitus_id = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_NUMBER_INT);
        $teksti = filter_input(INPUT_GET, 'teksti', FILTER_SANITIZE_STRING);
        try {
            $tietokanta = new PDO('mysql:host=localhost;dbname=blogi;charset=utf8', 'root', '');
            $tietokanta->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $sql = "INSERT INTO kommentti(teksti, kirjoitus_id, kayttaja_id) "
                    . "VALUES (:teksti, :kirjoitus_id, :kayttaja_id)";
            $kysely = $tietokanta->prepare($sql);
            $kysely->bindValue(":teksti", $teksti, PDO::PARAM_STR);
            $kysely->bindValue(":kirjoitus_id", $kirjoitus_id, PDO::PARAM_STR);
            $kysely->bindValue(":kayttaja_id", $_SESSION['kayttaja_id'], PDO::PARAM_STR);
            $kysely->execute();
            header("Location:blogi.php?id=$kirjoitus_id");
            exit;
        } catch (PDOException $pdoex) {
            print '<p>Tietokannan avaus epäonnistui.' . $pdoex->getMessage() . '</p>';
        }
    }
}
try {
    if ($_SERVER['REQUEST_METHOD'] === 'GET') {
        $tietokanta = new PDO('mysql:host=localhost;dbname=blogi;charset=utf8', 'root', '');
        $tietokanta->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $sql = "SELECT*, kirjoitus_id as id, kommentti.id as poisto_id FROM"
                . " kommentti INNER JOIN kayttaja ON kommentti.kayttaja_id = kayttaja_id"
                . " WHERE kirjoitus_id = $kirjoitus_id ORDER BY paivays desc";
        $kysely = $tietokanta->query($sql);
        if ($kysely) {
            print '<ul>';
            while ($tietue = $kysely->fetch()) {
                $kayttaja = $tietue['tunnus'];
                $paivays = $tietue['paivays'];
                $date = new DateTime($paivays);
                $poisto_id = $tietue['poisto_id'];

                print '<li>' . $tietue['teksti'] . " " . $date->format('d-m-Y H:i') . " by " . $tietue['tunnus'] . " ";
                if (isset($_SESSION['kayttaja_id'])) {
                    print '<a class="glyphicon glyphicon-trash" onclick="alert(\'Kommentti poistettu!\')" aria-hidden="true" href="blogi.php?id=' . $kirjoitus_id . '&poisto_id=' . $poisto_id . '"></a>';
                }
                print '</li>';
            }
            print '</ul>';
        }
    }
} catch (PDOException $pdoex) {
    print '<p>Tietokannan avaus epäonnistui.' . $pdoex->getMessage() . '</p>';
}
if (isset($_GET['poisto_id'])) {
    try {
        $poisto_id = filter_input(INPUT_GET, 'poisto_id', FILTER_SANITIZE_NUMBER_INT);
        $tietokanta = new PDO('mysql:host=localhost;dbname=blogi;charset=utf8', 'root', '');
        $tietokanta->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $kysely = $tietokanta->prepare("DELETE FROM kommentti WHERE id = '" . $poisto_id . "'");
        $kysely->bindValue(':id', $poisto_id, PDO::PARAM_INT);
        $kysely->execute();
        header("Location: blogi.php?id=$kirjoitus_id");
        if ($kysely->execute()){
                print('<p>Kommentti on poistettu!</p>');
            }
    } catch (Exception $ex) {
        print '<p>Tietokannan avaus epäonnistui.' . $pdoex->getMessage() . '</p>';
    }
}
?>



<?php include_once 'inc/bottom.php'; ?>

