<?php include_once 'inc/top.php'; ?>
<?php
$kayttaja_id = 1;
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    try {
        $tietokanta = new PDO('mysql:host=localhost;dbname=blogi;charset=utf8', 'root', '');

        $tietokanta->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        $id = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_NUMBER_INT);
        $otsikko = filter_input(INPUT_POST, 'otsikko', FILTER_SANITIZE_STRING);
        $teksti = filter_input(INPUT_POST, 'teksti', FILTER_SANITIZE_STRING);

        $kysely = $tietokanta->prepare("INSERT INTO kirjoitus(otsikko,teksti,kayttaja_id) VALUES (:otsikko,:teksti,:kayttaja_id)");
        $kysely->bindValue(':otsikko', $otsikko, PDO::PARAM_STR);
        $kysely->bindValue(':teksti', $teksti, PDO::PARAM_STR);
        $kysely->bindValue(':kayttaja_id', $kayttaja_id, PDO::PARAM_INT);
        $kysely->execute();
        if ($kysely) {
            print '<p style="text-align:center">Kirjoitus tallennettu!</p>';
        }
        }
     catch (PDOException $pdoex) {
        print '<p>Tietokannan avaus epäonnistui.' . $pdoex->getMessage() . '</p>';
    }
}
?>
<form method="post">
    <h2>Lisää kirjoitus</h2>
    <div class="form-group">
        <label for="otiskko">Otsikko</label>
        <input type="text" class="form-control" name="otsikko" placeholder="Otsikko tähän" required="required">
    </div>
    <div class="form-group">
        <label for="teksti">Teksti</label>
        <textarea rows="2" class="form-control" name="teksti" placeholder="Teksti tänne..." required="required"></textarea>
    </div>
    <button type="submit" class="btn btn-primary">Tallenna</button>
    <button class="btn btn-default" onclick="window.location='index.php'">Peruuta</button>
</form>
<?php include_once 'inc/bottom.php'; ?>